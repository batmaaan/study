package threads;
///////8.1 потоки никогда одновременно не изменят эту переменную

import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;

public class IncrementDecrement {
    static AtomicInteger x = new AtomicInteger(0);

    public static synchronized void main(String[] args) throws InterruptedException {
        Logger log = Logger.getLogger(String.valueOf(IncrementDecrement.class));

        Thread inc = new Thread(new Runnable() {
            @Override
            public void run() {

                for (int i = 0; i < 1_000000; i++) {
                    log.info("Добавляем");
                    x.incrementAndGet();

                }
            }
        });

        Thread dec = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 1_000000; i++) {
                    log.info("Вычитаем");
                    x.decrementAndGet();
                }
            }
        });
        inc.start();
        dec.start();
        Thread.sleep(15_000);
        System.out.println("result=" + x);
    }
}