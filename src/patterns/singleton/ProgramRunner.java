package patterns.singleton;

public class ProgramRunner {
    public static void main(String[] args) {
        NewsService.getNewsService().addNewsInfo("First log...");
        NewsService.getNewsService().addNewsInfo("Second log...");
        NewsService.getNewsService().addNewsInfo("Third log...");
        NewsService.getNewsService().addNewsInfo("Four log...");

        NewsService.getNewsService().showNews();
    }
}
