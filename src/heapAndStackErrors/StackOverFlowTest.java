package heapAndStackErrors;

public class StackOverFlowTest {


    public static void recursive(int i) {
        System.out.println("Номер" + i);
        if (i == 0)
            return;
        else
            recursive(++i);

    }

    public static void main(String[] args) {
            StackOverFlowTest.recursive(1);
    }
}
