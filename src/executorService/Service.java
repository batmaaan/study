package executorService;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class Service {
    public static void main(String[] args) throws InterruptedException {
        var executorService = new ScheduledThreadPoolExecutor(8);
        executorService.scheduleWithFixedDelay(() -> {
            System.out.println("hello world");
        }, 0, 5, TimeUnit.MINUTES);
    }
}
